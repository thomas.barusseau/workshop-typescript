# [WORKSHOP] TypeScript

## General introduction

TypeScript is a compiled language that's a superset of JavaScript.

It was developed by Microsoft and adds static typing with optional type annotations to JavaScript.

## Topics

### Basics of TypeScript

By adding type annotations to your function inputs/outputs as well as your variables, you
can develop a very robust codebase that is:

- Easier to understand
- Easier to maintain
- Easier to refactor

Types add much more than compile-time safety, they also help you understand what a function
expects, what it returns. It also helps you inspecting the code very easily instead of having
to guess what a variable might contain.

Its very permissive type system allows the creation of very powerful libraries, such as:

- [`fp-ts`](https://gcanti.github.io/fp-ts/), which brings verbose but somewhat functional programming to TypeScript. This is widely used at Swan.
- [`ts-morph`](https://ts-morph.com/), which allows for (very) easy TS AST navigation and manipulation.
- [`zod`](https://github.com/colinhacks/zod), which mixes both static and runtime type validation.
- [`ts-pattern`](https://github.com/gvergnaud/ts-pattern), an exhaustive pattern matching library with smart type inference.

The fact that functions are first-class citizens means that you can assign functions to
variables or pass them as parameters to other functions, which enables a lot of cool things,
all while keeping everything type-safe and with minimal verbosity.

### Compilation process

TypeScript code is compiled with the `tsc` CLI, although there are other candidates for it.

This crucial step transforms your `.ts` files in `.js` files, along with optional `.map` files
that allows tools to map between TS and JS code.

The general process when compiling is the following:

- The code is parsed into an AST (Abstract Syntax Tree)
- Types definitions are read (including the ones not defined in your files)
- Types are checked everywhere
- JavaScript code is emitted, along with optional map files

The output of the compilation process is heavily dependant on what you have inside your
`tsconfig.json` file.

### TypeScript configuration

> TODO: Complete this part!

All of the TypeScript configuration for your project is contained within the `tsconfig.json` file.

### Generics

> TODO: Complete this part!

TypeScript offers a pretty complete type system that includes generics.

Generic types (or interfaces, functions...) are placeholders that can represent multiple type
while retaining information about the type (as opposed to `any`, for example).

An easy example would be this javascript function, that takes a list of numbers... TO COMPLETE

### Advanced types

> TODO: Complete this part!

### Interfacing with JavaScript code

> TODO: Complete this part!

Talk about: definition files, `DefinitelyTyped`

## Additional resources

- [Official documentations](https://www.typescriptlang.org/docs/)
- [Awesome TypeScript repository](https://github.com/dzharii/awesome-typescript)
- 

## Toolings around TypeScript

> TODO: Complete this part!

## In practice

- Create an empty TypeScript project using `tsc --init`
- Modify the configuration to make `tsc` stricter
- Configure the compilation options
- Make sure that everything compiles accordingly
- Write a basic function that sums two numbers together
- Write an interface that describes a person with a first name, a last name, and an optional age
- Write a function that greets a person
- Write a function that takes a list of numbers and sums them together
- Write a function that takes two numbers, one of which is optional
- Write a constant number variable, as well as a regular number variable, see the differences
- Write a constant object variable (with a specific type), and see how it works
- Write an enum of several countries. Write an union of several countries. What are the differences? Explanation [here](https://stackoverflow.com/a/40279394/11793087).
- Write an [iterable union type](https://stackoverflow.com/a/60041791/11793087) to replace the previous two
- Write another interface that describes a pet with a name and a kind, which should be an union of multiple string values
- What are the differences between types and interfaces? Details [here](https://www.typescriptlang.org/docs/handbook/advanced-types.html#interfaces-vs-type-aliases)
- Write a generic function that takes an array of any type and logs it to the console
- Write a type using the `keyof` operator on one of the previously defined types
- Declare a variable type using the `typeof` operator
- This next section is about "utility types", which provide common type transformations. The documentation is [here](https://www.typescriptlang.org/docs/handbook/utility-types.html).
- Write a function that takes in a person input, but with all fields as optionals: `Partial<T>`
- Write a function that takes in a person input, but with all fields as required: `Required<T>`
- Write a function that requires a person input, without the age: `Omit<T, Keys>`
- Write a function that requires a person input, with only the age: `Pick<T, Keys>`
- Write an interface that can take any properties of string types, with only number values: `Record<Keys, Type>`

## Workshop

> TODO: Complete this part!

To play around a bit more with TypeScript, create a fork of this repository.

Inside the `src/` directory, you will find sources that are in untyped TypeScript.
Your objective is to type everything to make sure that you are confident enough in
your ability to manipulate basic and advanced types.
